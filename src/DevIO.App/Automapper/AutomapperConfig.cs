﻿using AppMvcBasica.Models;
using AutoMapper;
using DevIO.App.ViewModels;

namespace DevIO.App.Automapper
{
    public class AutomapperConfig : Profile
    {
        public AutomapperConfig() 
        { 
            CreateMap<Fornecedor, FornecedorViewModel>().ReverseMap();
            CreateMap<Endereco, EnderecoViewModel>().ReverseMap();
            CreateMap<Produto, ProdutoViewModel>().ReverseMap();
        }
    }
}
